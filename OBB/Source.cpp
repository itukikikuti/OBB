#include "XLibrary11.hpp"
using namespace XLibrary11;

float Dot(Float2 a, Float2 b)
{
	Float2 dot = XMVector2Dot(a, b);
	return dot.x;
}

bool CheckHitAxis(Float3 posA, float halfSizeA, float angleA, Float3 posB, Float3 halfSizeB, float angleB)
{
	float radianA = angleA / 180.0f * 3.14f;
	Float2 radianB = Float2(
		angleB / 180.0f * 3.14f,
		(angleB + 90.0f) / 180.0f * 3.14f
	);

	Float2 a = Float2(cosf(radianA), sinf(radianA));
	Float2 bx = Float2(cosf(radianB.x), sinf(radianB.x)) * halfSizeB.x;
	Float2 by = Float2(cosf(radianB.y), sinf(radianB.y)) * halfSizeB.y;

	float projectA = halfSizeA;
	float projectB = abs(Dot(a, bx)) + abs(Dot(a, by));
	float projectAB = abs(Dot((Float2)(posB - posA), a));

	if (projectA + projectB > projectAB)
	{
		return true;
	}

	return false;
}

bool CheckHitOBB(Float3 posA, Float3 halfSizeA, float angleA, Float3 posB, Float3 halfSizeB, float angleB)
{
	if (CheckHitAxis(posA, halfSizeA.x, angleA, posB, halfSizeB, angleB) &&
		CheckHitAxis(posA, halfSizeA.y, angleA + 90.0f, posB, halfSizeB, angleB) &&
		CheckHitAxis(posB, halfSizeB.x, angleB, posA, halfSizeA, angleA) &&
		CheckHitAxis(posB, halfSizeB.y, angleB + 90.0f, posA, halfSizeA, angleA))
	{
		return true;
	}

	return false;
}

int Main()
{
	Camera camera;
	camera.color = Float4(0.0f, 0.0f, 0.0f, 0.0f);

	Sprite sprite(L"block.png");

	Float3 posA = Float3(0.0f, 0.0f, 0.0f);
	Float3 posB = Float3(30.0f, 90.0f, 0.0f);
	Float3 scaleA = Float3(8.0f, 0.5f, 1.0f);
	Float3 scaleB = Float3(5.0f, 1.0f, 1.0f);
	float angleA = 0.0f;
	float angleB = 30.0f;

	while (Refresh())
	{
		camera.Update();

		angleA += 1.0f;
		if (Input::GetKey(VK_LEFT))
			posB.x -= 1.0f;

		if (Input::GetKey(VK_RIGHT))
			posB.x += 1.0f;

		if (Input::GetKey(VK_UP))
			posB.y += 1.0f;

		if (Input::GetKey(VK_DOWN))
			posB.y -= 1.0f;

		if (CheckHitOBB(posA, scaleA * 8.0f, angleA, posB, scaleB * 8.0f, angleB))
		{
			sprite.color = Float4(1.0f, 0.0f, 0.0f, 1.0f);
		}
		else
		{
			sprite.color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		}

		sprite.position = posA;
		sprite.angles.z = angleA;
		sprite.scale = scaleA;
		sprite.Draw();

		sprite.position = posB;
		sprite.angles.z = angleB;
		sprite.scale = scaleB;
		sprite.color = Float4(1.0f, 1.0f, 1.0f, 1.0f);
		sprite.Draw();
	}
}
